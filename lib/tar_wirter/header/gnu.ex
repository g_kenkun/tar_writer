defmodule DockerApi.Header.Gnu do
                                                      # Offset    Long
  defstruct name:     String.duplicate(<<0>>, 100),   #   0 byte  100 byte
            mode:     String.duplicate(<<0>>, 8),     # 100 byte    8 byte
            uid:      String.duplicate(<<0>>, 8),     # 108 byte    8 byte
            gid:      String.duplicate(<<0>>, 8),     # 116 byte    8 byte
            size:     String.duplicate(<<0>>, 12),    # 124 byte   12 byte
            mtime:    String.duplicate(<<0>>, 12),    # 136 byte   12 byte
            chksum:   String.duplicate(<<32>>, 8),    # 148 byte    8 byte
            typeflag: String.duplicate(<<0>>, 1),     # 156 byte    1 byte
            linkname: String.duplicate(<<0>>, 100),   # 157 byte  100 byte
            magic:    <<117, 115, 116, 97, 114, 00>>, # 257 byte    6 byte
            version:  <<48, 48>>,                     # 263 byte    2 byte
            uname:    String.duplicate(<<0>>, 32),    # 265 byte   32 byte
            gname:    String.duplicate(<<0>>, 32),    # 297 byte   32 byte
            devmajor: String.duplicate(<<0>>, 8),     # 329 byte    8 byte
            devminor: String.duplicate(<<0>>, 8),     # 337 byte    8 byte
            atime:    String.duplicate(<<0>>, 12),
            ctime:    String.duplicate(<<0>>, 12),
            offset:    String.duplicate(<<0>>, 12),
            longnames:    String.duplicate(<<0>>, 12),
            unused_pad:    String.duplicate(<<0>>, 12)

  @moduledoc false
end
