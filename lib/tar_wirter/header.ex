defmodule DockerApi.Header do
  defmodule PosixHeader do
    defstruct name:     String.duplicate(<<0>>, 100),   #   0 byte  100 byte
              mode:     String.duplicate(<<0>>,   8),   # 100 byte    8 byte
              uid:      String.duplicate(<<0>>,   8),   # 108 byte    8 byte
              gid:      String.duplicate(<<0>>,   8),   # 116 byte    8 byte
              size:     String.duplicate(<<0>>,  12),   # 124 byte   12 byte
              mtime:    String.duplicate(<<0>>,  12),   # 136 byte   12 byte
              chksum:   String.duplicate(<<32>>,  8),   # 148 byte    8 byte
              typeflag: String.duplicate(<<0>>,   1),   # 156 byte    1 byte
              linkname: String.duplicate(<<0>>, 100),   # 157 byte  100 byte
              magic:    String.duplicate(<<0>>,   6),   # 257 byte    6 byte
              version:  String.duplicate(<<0>>,   3),   # 263 byte    2 byte
              uname:    String.duplicate(<<0>>,  32),   # 265 byte   32 byte
              gname:    String.duplicate(<<0>>,  32),   # 297 byte   32 byte
              devmajor: String.duplicate(<<0>>,   8),   # 329 byte    8 byte
              devminor: String.duplicate(<<0>>,   8),   # 337 byte    8 byte
              prefix:   String.duplicate(<<0>>, 155)    # 345 byte  155 byte
    @magic   <<117, 115, 116, 97, 114, 0>>
    @version <<48, 48>>
  end

  defmodule Sparse do
    defstruct offset: String.duplicate(<<0>>,   12),
              numbytes: String.duplicate(<<0>>, 12)
  end

  defmodule SparseHeader do
    defstruct sp:  List.duplicate(%Sparse{}, 21),      #   0 byte  504 byte
              isextended: String.duplicate(<<0>>, 1)   # 504 byte    1 byte
  end

  defmodule OldGnuHeader do
    defstruct name:       String.duplicate(<<0>>, 100),   #   0 byte  100 byte
              mode:       String.duplicate(<<0>>,   8),   # 100 byte    8 byte
              uid:        String.duplicate(<<0>>,   8),   # 108 byte    8 byte
              gid:        String.duplicate(<<0>>,   8),   # 116 byte    8 byte
              size:       String.duplicate(<<0>>,  12),   # 124 byte   12 byte
              mtime:      String.duplicate(<<0>>,  12),   # 136 byte   12 byte
              chksum:     String.duplicate(<<32>>,  8),   # 148 byte    8 byte
              typeflag:   String.duplicate(<<0>>,   1),   # 156 byte    1 byte
              linkname:   String.duplicate(<<0>>, 100),   # 157 byte  100 byte
              magic:      String.duplicate(<<0>>,   6),   # 257 byte    6 byte
              version:    String.duplicate(<<0>>,   3),   # 263 byte    2 byte
              uname:      String.duplicate(<<0>>,  32),   # 265 byte   32 byte
              gname:      String.duplicate(<<0>>,  32),   # 297 byte   32 byte
              devmajor:   String.duplicate(<<0>>,   8),   # 329 byte    8 byte
              devminor:   String.duplicate(<<0>>,   8),   # 337 byte    8 byte
              atime:      String.duplicate(<<0>>,  12),   # 345 byte   12 byte
              ctime:      String.duplicate(<<0>>,  12),   # 357 byte   12 byte
              offset:     String.duplicate(<<0>>,  12),   # 369 byte   12 byte
              longnames:  String.duplicate(<<0>>,   4),   # 381 byte   4 byte
              unused_pad: String.duplicate(<<0>>,   1),   # 385 byte   1 byte
              sp:         List.duplicate(%Sparse{}, 4),   # 386 byte  96 byte
              isextended: String.duplicate(<<0>>,   1),   # 482 byte   1 byte
              realsize:   String.duplicate(<<0>>,  12)    # 483 byte  12 byte
    @magic   <<117, 115, 116, 97, 114, 32>>
    @version <<32, 0>>
  end



  # Values used in typeflag field
  @regtype  <<48>>
  @linktype <<49>>
  @symtype  <<50>>
  @chrtype  <<51>>
  @blktype  <<52>>
  @dirtype  <<53>>
  @fifotype <<54>>
  @conttype <<55>>
  @xhdtype  <<120>>
  @xgltype  <<103>>

  # Bits used in the mode field, values in octal.
  @tsuid <<48, 52, 48, 48, 48>>
  @tsgid <<48, 50, 48, 48, 48>>
  @tsvtx <<48, 49, 48, 48, 48>>

  @turead  <<48, 48, 52, 48, 48>>
  @tuwrite <<48, 48, 50, 48, 48>>
  @tuexec  <<48, 48, 49, 48, 48>>
  @tgread  <<48, 48, 48, 52, 48>>
  @tgwrite <<48, 48, 48, 50, 48>>
  @tgexec  <<48, 48, 48, 49, 48>>
  @toread  <<48, 48, 48, 48, 52>>
  @towirte <<48, 48, 48, 48, 50>>
  @toexec  <<48, 48, 48, 48, 49>>



  @doc """
  """
  @doc since: "0.1.0"

  def create(file_stat = %File.Stat{}) do

    mode = file_stat.mode
    mtime = file_stat.mtime
    size = file_stat.size
    uid  = file_stat.uid
    gid  = file_stat.gid
  end

  defp type_flag(:device),    do: <<>>
  defp type_flag(:directory), do: <<53>>
  defp type_flag(:regular),   do: <<48>>
  defp type_flag(:other),     do: <<>>
  defp type_flag(:symlink),   do: <<50>>

  defp mode()
end
